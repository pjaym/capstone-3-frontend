import React,  { useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';  
import Router from 'next/router';
import { useRouter } from 'next/router';
import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-pro-sidebar/dist/css/styles.css';
import { UserProvider } from '../UserContext';
import AppHelper from '../helper/app-helper';
import CustomLoadingScreen from '../components/CustomLoadingScreen';
import Loader from '../components/Loader';
import Navbar from '../components/Navbar';

function MyApp({ Component, pageProps }) {

	const [isLoading, setIsLoading] = useState(false);

	// State hook for the user details
	const [user, setUser] = useState({
		// Initialized as an object with properties from localStorage
		email: null,
		id: null,
		token: null,	
		// isAdmin: null
	})

	// Function for clearing the localStorage
	const unsetUser = () => {
		localStorage.clear();

		// Changes the value of the user state back to it's original value
		setUser({ 
			email: null,
			id: null,
			token: null,
			// isAdmin: null

		});
	}




	Router.onRouteChangeStart = () => {
	    console.log('onRouteChangeStart Triggered');
	   	<CustomLoadingScreen />;
	   	setIsLoading(true)
	};

	Router.onRouteChangeComplete = () => {
	    console.log('onRouteChangeComplete Triggered');
	    <CustomLoadingScreen />;
	    setIsLoading(false)
	};

	Router.onRouteChangeError = () => {
	    console.log('onRouteChangeError Triggered');	    
	    <CustomLoadingScreen />;
	    setIsLoading(true)

	};

	//console.log(user);

	useEffect(() => {

		setUser({
			email: localStorage.getItem('email'),
			id: localStorage.getItem('id'),
			token: localStorage.getItem('token'),
			// Added a condition to convert the string data type into boolean
			// isAdmin: localStorage.getItem('isAdmin') ===  'true'
		})		

	}, [])

	const router = useRouter();		
	

  	return (
  		<React.Fragment>
  			<UserProvider value={{user, setUser, unsetUser}}>
  				<div className="app w-100">  							
  					<Container fluid className="py-5 mx-2">  						
  						<Navbar />
  						<Col className="py-5">						
  							
  							{isLoading 
  								? 	<CustomLoadingScreen />  								
  							 	: 	<Component {...pageProps} />
  							}
  							
  						</Col>	  					
	  				</Container>
  				</div>  											
  			</UserProvider>
  		</React.Fragment>
  	)
}

export default MyApp

//<Sidebar /> 