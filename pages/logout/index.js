import React, { useContext, useEffect } from 'react';
import Router from 'next/router';
import UserContext from '../../UserContext';
import Loader from  '../../components/Loader';

export default function index() {

	const { unsetUser } = useContext(UserContext);
	
	useEffect(() => {
		// Clears the localStorage of user information
		unsetUser();
		// Redirects to login page
		Router.push('/login')

	}) //remove empty array because of redirection	

	return (

		<Loader />
	)

}