import React, { useState, useEffect, useContext } from 'react';
import Head from 'next/head';
import Router from 'next/router';
import Link from 'next/link';
import { Form, Button, Row, Col, Card } from 'react-bootstrap';
import { GoogleLogin } from 'react-google-login';
import View from  '../../components/View';
import AppHelper from '../../helper/app-helper';
import Alert from '../../helper/alert';
import UserContext from '../../UserContext';

export default function index() {
    return (
        <View title={ 'Login' }>
            <Row className="justify-content-center">
                <Col xs md="6">                   
                    <LoginForm/>
                </Col>
            </Row>
        </View>
    )
}

const LoginForm = () => {

    const { user, setUser } = useContext(UserContext);
    
    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);

    function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json'},
            body:JSON.stringify({
                email: email,
                password: password
            })
        }
        
        fetch(`${AppHelper.API_URL}/users/login`, options)
        .then(AppHelper.toJSON)
        .then(data => {
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
                Alert("Successfully signed in", "success")
            }else {
                if (data.error === 'does-not-exist' || data.error === 'deleted-user') {
                    Alert('User does not exist!','error')
                }else if(data.error === 'incorrect-password') {
                    Alert('Password is incorrect.', 'error')
                }else if(data.error === 'login-type-error') {
                        Alert('You may have registered through a different login procedure, try the alternative login procedures.', 'error')
                }
            }
        })
    }

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);

    const authenticateGoogleToken = (response) => {

        console.log(response);

        const payload = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json'},
            body: JSON.stringify({ tokenId: response.tokenId})
        }

        fetch(`${AppHelper.API_URL}/users/verify-google-id-token`,  payload)
        .then(AppHelper.toJSON)
        .then(data => {

            if(typeof data.accessToken !== 'undefined'){
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            }else {
                if(data.error == 'google-auth-error'){
                    Alert(                        
                        'Google authentication procedure failed', 'error')
                }else if(data.error === 'login-type-error') {
                    Alert('You may have registered through a different login procedure', 'error')
                }
            }
        })
    }

    const retrieveUserDetails = (accessToken) => {
        const options = {
            headers: { Authorization:  `Bearer ${ accessToken}`}
        }

        fetch(`${ AppHelper.API_URL}/users/details`, options)
        .then(AppHelper.toJSON)
        .then(data=> {
            setUser({id: data._id})
            setUser({token: accessToken})
            AppHelper.token = accessToken
            Router.push('/transactions')
        })
    }


    return (
        <React.Fragment>
            <Card className="px-3 mx-3 bg-light">
                <Card.Body>
                    <Card.Title className="text-center text-info py-3">Login</Card.Title>
                        <Form onSubmit={(e) => authenticate(e)}>
                            <Form.Group controlId="userEmail" className="bg-light">                                
                                <Form.Control 
                                    type="email" 
                                    placeholder="Enter email"
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value)}
                                    required
                                />
                            </Form.Group>
                            <Form.Group controlId="password">                                
                                <Form.Control 
                                    type="password" 
                                    placeholder="Password"
                                    value={password}
                                    onChange={(e) => setPassword(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            {isActive ? 
                                <React.Fragment>                    
                                <Button variant="info" type="submit" id="submitBtn" className="w-100 text-center d-flex justify-content-center my-3">
                                    Login
                                </Button>
                                <p className="text-center text-info">or</p>
                                <GoogleLogin
                                    // clientId = OAuthClient id from Cloud Google developer platform
                                    clientId="1002389236412-mqm75ishpl4vu6pbnf2autdv2o3c1j2c.apps.googleusercontent.com"
                                    buttonText="Connect with Google"
                                    // onSuccess = it runs a function w/c returns a Google user object w/c provides access to all of the Google user method and details
                                    onSuccess={ authenticateGoogleToken }
                                    onFailure={ authenticateGoogleToken } // you can modify this part { failed }
                                    // cookiePolicy = determines cookie policy for the origin of the google login requests
                                    cookiePolicy={'single_host_origin'}
                                    className="w-100 text-center d-flex justify-content-center my-3 btn btn-outline-danger"                                
                                />
                                </React.Fragment>
                                : 
                                <React.Fragment>
                                <Button variant="info" type="submit" id="submitBtn" disabled className="w-100 text-center d-flex justify-content-center my-3 google-btn">
                                    Login
                                </Button>
                                <p className="text-center text-info">or</p>
                                <GoogleLogin
                                    // clientId = OAuthClient id from Cloud Google developer platform
                                    clientId="1002389236412-mqm75ishpl4vu6pbnf2autdv2o3c1j2c.apps.googleusercontent.com"
                                    buttonText="Connect with Google"
                                    // onSuccess = it runs a function w/c returns a Google user object w/c provides access to all of the Google user method and details
                                    onSuccess={ authenticateGoogleToken }
                                    onFailure={ authenticateGoogleToken } // you can modify this part { failed }
                                    // cookiePolicy = determines cookie policy for the origin of the google login requests
                                    cookiePolicy={'single_host_origin'}
                                    className="w-100 text-center d-flex justify-content-center my-3 text-danger"                            
                                />
                                </React.Fragment>
                            }
                            <Link href="/register">
                                <small className="my-3 text-secondary">Don't have an account? <span className="text-info">Register</span></small>
                            </Link>
                        </Form>

                </Card.Body>                
            </Card>           
        </React.Fragment>
    )
}
// global variable
/* ///
localStorage {
    email: "arvin@mail.com"
}
*/