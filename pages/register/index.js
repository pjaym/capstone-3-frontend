import React, { useState, useEffect } from 'react';
import { Form, Button, Card, Row, Col } from 'react-bootstrap';
import Router from 'next/router';
import Link from 'next/link';
import AppHelper from '../../helper/app-helper';
import Alert from '../../helper/alert';
import View from  '../../components/View';

export default function index() {

	//State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// state to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	//check if values are successfully binded
	// console.log(email);
	// console.log(password1);
	// console.log(password2);

	function registerUser(e) {

		// Prevents page redirection via form submission
		e.preventDefault();

		if(password1 === password2 && email !== '' && firstName !== '' && lastName !== '') {

			const options = {
	            method: 'POST',
	            headers: { 'Content-Type': 'application/json'},
	            body:JSON.stringify({
	                email: email,
	                password: password1,
	                firstName: firstName,
	                lastName: lastName   

	            })
            }

            fetch(`${AppHelper.API_URL}/users/email-exists`, options)
	        .then(AppHelper.toJSON)
	        .then(email => {

	        	// console.log(`Email-exist: ${email}`)        	

	        	if(email) {
	        		Alert("Registration failed! You are already registered!", "error")
	        		Router.push('/login')

	        	}else {
	        		fetch(`${AppHelper.API_URL}/users`, options)
			        .then(AppHelper.toJSON)
			        .then(data => {

			        	Alert('Registration successful! Login to start tracking your expenses.', 'success')
			        	// console.log(`User registration!: ${data}`)
			        	console.log("Thank you for registering!")
			        	Router.push('/login')

			        })
	        	}

	        })     

        	

			//Router.push('/login')   
        
		}else {

			// Clear input fields
			setEmail('');
			setPassword1('');
			setPassword2('');
			setFirstName('');
			setLastName('');
			Alert("All fields are required", "error")
		} 		
	}

	useEffect(() => {

		// Validation to enable subbmit button when all fields are populated and both passwords match
		if((email !== "" && password1 !== "" && password2  !== "" && firstName  !== "" && lastName  !== "") && (password1 === password2)) {

			setIsActive(true);


		}
		else {

			setIsActive(false);
		}

	}, [email, password1, password2, firstName,  lastName])

	return (
			<React.Fragment>
				<View title={ 'Register' }></View>
				<Row className="justify-content-md-center">
					<Card className="px-3 mx-3 bg-light"  style={{ width: '35rem' }}>		  
					  <Card.Body>
					    <Card.Title className="text-info text-center py-3">Register</Card.Title>			    
					    	<Form onSubmit={(e) => registerUser(e)}>
								<Form.Group controlId="firstName">							
									<Form.Control
										type="text"
										placeholder="Enter your first name"
										value={firstName}
										onChange={(e) => setFirstName(e.target.value)}
										required
									/>				
								</Form.Group>
								<Form.Group controlId="lastName">							
									<Form.Control
										type="text"
										placeholder="Enter your last name"
										value={lastName}
										onChange={(e) => setLastName(e.target.value)}
										required
									/>				
								</Form.Group>
								<Form.Group controlId="userEmail">							
									<Form.Control
										type="email"
										placeholder="Enter email"
										value={email}
										onChange={(e) => setEmail(e.target.value)}
										required
									/>
									<Form.Text className="text-muted">
										We'll never share your email with anyone else.
									</Form.Text>
								</Form.Group>
								<Form.Group controlId="password1">							
									<Form.Control
										type="password"
										placeholder="Password"
										value={password1}
										onChange={(e) => setPassword1(e.target.value)}
										required
									/>				
								</Form.Group>
								<Form.Group controlId="password2">							
									<Form.Control
										type="password"
										placeholder="Verify password"
										value={password2}
										onChange={(e) => setPassword2(e.target.value)}
										required
									/>
								</Form.Group>

								{isActive ?

									<Button variant="info" type="submit" id="submitBtn" className="my-3" block>
									Register
									</Button>
									:
									<Button variant="info" type="submit" id="submitBtn" className="my-3" disabled block>
									Register
									</Button>
								}
								<Link href="/login">
                                <small className="my-3 text-secondary">Already have an account? <span className="text-info">Login here!</span></small>
                            	</Link>						
							</Form>			  
					  	</Card.Body>
					</Card>
				</Row>
			</React.Fragment>
		
	)
}

// Avoid using this syntax when passing event in a function
//registerUser() Ex. registerUser(e) //