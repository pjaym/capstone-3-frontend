import React, { useState, useEffect, useContext } from  'react';
import { Jumbotron, Container, Card, Button } from 'react-bootstrap';
import Image from 'next/image';
import Router from 'next/router';
import path from  'path';
import Link from 'next/link';
import { BrowserView, MobileView } from "react-device-detect";
import AppHelper from '../helper/app-helper';
import UserContext from '../UserContext';

const data = {

    title: "SpendRite",
    content: "Track your expenses and eliminate wasteful spending habits in your financial life.",
    destination: "/transactions",
    label: ""
  }

export default function Home() {

  const { user, setUser } = useContext(UserContext);

  const [token, setToken] = useState(user.token);
  const [isLoggedIn, setIsLoggedIn] = useState(false);


  function getToken() {
    setToken(localStorage.getItem('token'))

    if(token !== null) {
      setIsLoggedIn(true)
    }else {
      setIsLoggedIn(false)
    }     
  }

  useEffect(() => {   
    if(token !== null) {
      setIsLoggedIn(true)
    }else {
      setToken(localStorage.getItem('token'))
      
    }
  }, [])

  useEffect(() => {

    if(token === null) {
      setIsLoggedIn(false)      
    }   
  }, [token])


  console.log(token)

  useEffect(() => {
      window.addEventListener("beforeunload", getToken());
      return () => {
        window.removeEventListener("beforeunload", getToken());
      };
  }, [token]);

  return (
    <React.Fragment>      
      <BrowserView>
         <Container className="mt-5 pt-4 mb-4">
            <Card className="bg-light text-white w-80">
               <Image
                      src="/home.png"
                      alt="Icon"
                      width={850}
                      height={600}
                />          
              <Card.ImgOverlay>
                <Card.Title className="text-info display-1 fw-bold mt-5">SpendRite</Card.Title>
                <Card.Text className="text-dark mb-0 lead">
                  Track your expenses and eliminate wasteful
                </Card.Text>
                <Card.Text className="text-dark lead">
                  spending habits in your financial life.
                </Card.Text>                
                {isLoggedIn 

                    ? 
                      <Link href="/transactions">
                        <Button variant="info">Go Back to transactions</Button>
                      </Link>
                    :
                      <React.Fragment>
                        <Link href="/register">
                          <Button variant="info">Register</Button>
                        </Link>
                        <Card.Text className="text-dark lead">
                          <Link href="/login">
                            <small className="my-3 mt-3 text-info"> or Login here!</small>
                          </Link>
                        </Card.Text> 
                      </React.Fragment>
                  }                          
              </Card.ImgOverlay>
            </Card>
          </Container>
        </BrowserView>
        <MobileView>       
            <Card className="mt-1 pt-1">
              <Image
                  src="/homeMobile.png"
                  alt="Icon"
                  width={750}
                  height={600}
              /> 
              <Card.Body>
                  <Card.Title>SpendRite</Card.Title>
                  <Card.Text className="text-dark lead">
                    Track your expenses and eliminate wasteful spending habits in your financial life.            
                  </Card.Text>
                  {isLoggedIn || token  !== null

                    ? 
                      <Link href="/transactions">
                        <Button variant="info">Go Back to transactions</Button>
                      </Link>
                    :
                      <React.Fragment>
                        <Link href="/register">
                          <Button variant="info">Register</Button>
                        </Link>
                        <Card.Text className="text-dark lead">
                          <Link href="/login">
                            <small className="my-3 mt-3 text-info"> or Login here!</small>
                          </Link>
                        </Card.Text> 
                      </React.Fragment>
                  }
                  
                </Card.Body>
            </Card>
        </MobileView>
    </React.Fragment>
  )

}
