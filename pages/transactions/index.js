import React, { useState, useEffect, useContext } from 'react';
import { Row, Col, Form, Spinner } from 'react-bootstrap';
import * as FaIcons from 'react-icons/fa';
import AppHelper from '../../helper/app-helper';
import UserContext from  '../../UserContext';
import AddTransaction from  '../../components/AddTransaction';
import TransactionDashboard from  '../../components/TransactionDashboard';
import Transaction from  '../../components/Transaction';
import SearchAndFilter from '../../components/SearchAndFilter';
import BottomNav from '../../components/BottomNav';
import Loader from '../../components/Loader';


export default function index() {

	const { user } = useContext(UserContext);

	const [token, setToken] = useState(user.token);
	const [userData, setUserData] = useState({});
	const [userTransaction, setUserTransaction] = useState([]);
	const [transactions, setTransactions] = useState('');
	const [categories, setCategories] = useState('');
	const [reload, setReload] = useState(false);
	const [show, setShow] = useState(false);
	const [isLoading, setIsLoading] = useState(true);
	const [searchAndFilter, setSearchAndFilter] = useState('');
	const [filteredTransaction, setFilteredTransaction] = useState(userTransaction);
	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');


	// console.log(filteredTransaction)
	// Function to reload/refresh data upon update
	function reloadData() {
		
		reload ? setReload(false) : setReload(true);
		// console.log("paging reload delete transaction")
	}

	// Function for search and filter
	function searchAndFilterData(data){
		data.length > 0 ? setFilteredTransaction(data) : setFilteredTransaction([])
		// console.log(data)
	}

	

	// Get token from local storage
	useEffect(() => {
		setToken(localStorage.getItem('token'))
	}, [])

	//  Get user details
	useEffect(() => {
		
		const options = {
            headers: { Authorization:  `Bearer ${ token }`}
        }

        fetch(`${AppHelper.API_URL}/users/details`, options)
			        .then(AppHelper.toJSON)
			        .then((data, err) => {

			        if (data) {
			        	setUserData(data);
			        }else {
			        	// Loader()
			        }

			        if(err) {

			        	// Loader()
			        }
			        
		})


	}, [token, reload])


	// Get transactions of the user
	useEffect(() => {		

		if (userData.transactions !== undefined) {

			let tempUserTransactions = [];

			userData.transactions.map((userTransaction, index) => {

				fetch(`${AppHelper.API_URL}/transactions/${userTransaction.transactionId}`)
		        .then(AppHelper.toJSON)
		        .then(transaction => {

		        	fetch(`${AppHelper.API_URL}/categories/${transaction.categoryId}`)
			        .then(AppHelper.toJSON)
			        .then(category => {

				        transaction.name = category.name;
		    			transaction.categoryIcon = category.iconPath;

		    			tempUserTransactions.push(transaction);	

		    			if(tempUserTransactions.length == userData.transactions.length) {	

		    				tempUserTransactions.sort(function(a, b) {
							    let dateA = new Date(a.transactionDate), dateB = new Date(b.transactionDate);
							    return dateB - dateA;
							});

							setUserTransaction(tempUserTransactions);					
							setFilteredTransaction(tempUserTransactions);					
						

						}else {
							<Loader />
						}

					})

			    })
			})

				
		}else {

			// console.log("no  data")
			return <Loader />

		}

	}, [userData])


	// Render the transactions
	useEffect(() => {	

		if (filteredTransaction.length > 0) {
			setTransactions(filteredTransaction.map((transaction) => {

				if(!transaction.isDeleted) {
					// console.log(transaction)
					return(
						 <Transaction key={transaction._id} transaction={transaction} reload={reloadData}/>
					);

				}else {
					return null
				}
			
			}))

		}else if(filteredTransaction.length === 0) {
			return setTransactions(null)
		}

		// console.log(transactions)

	}, [filteredTransaction, reload])

	useEffect(() => {


		if(userTransaction !== undefined && userTransaction.length >= 0) {
		 
			setSearchAndFilter(() => <SearchAndFilter data={userTransaction} searchAndFilter={searchAndFilterData} reload={reloadData} />);	
			

		}else {
			// setSearchAndFilter(() => <SearchAndFilter data={[]} searchAndFilter={searchAndFilterData} reload={reloadData} />);
			return null
		}

	}, [userTransaction])


	return (
		<React.Fragment>
			{userTransaction.length > 0 && userTransaction !== undefined 

				? 	
					<React.Fragment>
						<Form.Row>
							{searchAndFilter}
						</Form.Row>			
						{filteredTransaction.length > 0 && filteredTransaction !== undefined
							? 
								<TransactionDashboard userTransaction={filteredTransaction}/>
							:
								<TransactionDashboard />
							}
						
						<Row className="mx-1">
							{transactions}							
						</Row>						
					</React.Fragment>
				:
					<React.Fragment>
						<h1 className="display-1 text-center text-secondary mt-5"> <FaIcons.FaGrinBeamSweat/> </h1>
						<h3 className="display-5 text-center text-secondary"> There are no transactions yet.</h3>
						<h3 className="display-5 text-center text-secondary"> Tap the + button to add your first transaction. </h3>						
					</React.Fragment>	
			}
			{token 	? <AddTransaction reload={reloadData}/>  : null}						
		</React.Fragment>		
	)
}

		

						        

			        
		
			        
			    								
			

							
		
