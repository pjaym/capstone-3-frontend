import React, { useState, useEffect } from 'react';
import { Card, Button, Container, Row, Col, ListGroup, ListGroupItem, Form } from 'react-bootstrap';
import Modal from 'react-bootstrap/Modal'
import Router from 'next/router';
import * as FaIcons from 'react-icons/fa';
import { MobileView } from "react-device-detect";
import AppHelper from '../../helper/app-helper';
import Alert from '../../helper/alert';
import Loader from  '../../components/Loader';
import AddTransaction from  '../../components/AddTransaction';
import Navbar from '../../components/Navbar';

export default function index() {


	const [token, setToken] = useState(null)
	const [name, setName] = useState('')
	const [email, setEmail] = useState('')
	const [id, setId] = useState('')
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);
	const [upFirstName, setUpFirstName] = useState('');
	const [upLastName, setUpLastName] = useState('');
	const [upEmail, setUpEmail] = useState('');
	const [categoriesCount, setCategoriesCount] = useState(0);
	const [transactionsCount, setTransactionsCount] = useState(0);
	const [reload, setReload] = useState(false);
	
 	
 	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	const [showConfirm, setShowConfirm] = useState(false);

	const handleCloseConfirm = () => setShowConfirm(false);
	const handleShowConfirm = () => setShowConfirm(true);

    function reloadData() {	

    	reload ? setReload(false) : setReload(true)
	
	}

	function updateProfile() {

		
		if(upEmail !== '' && upFirstName !== '' && upLastName !== '') {
		const userOptions = {
            method: 'PUT',
            headers: { 
            			'Content-Type': 'application/json',
            			Authorization:  `Bearer ${ AppHelper.getAccessToken() }`
        			},
            body:JSON.stringify({
                email: upEmail,	                
                firstName: upFirstName,
                lastName: upLastName   

            })
    	}	

    	fetch(`${AppHelper.API_URL}/users/${id}`, userOptions)
        .then(AppHelper.toJSON)
        .then(data => {

        	// console.log(`Email-exist: ${email}`)        	

        	if(data) {
        		Alert("Profile successfully updated!", "success")
        		// Router.push('/login')
        		handleClose()
        		reloadData()

        	}else {

	        	Alert('Error found!', 'error')
	        	// console.log(`User registration!: ${data}`)
		    }	        	

		})

		}else {

			// Clear input fields
			setUpEmail(email);
			setUpFirstName(firstName);
			setUpLastName(lastName);
			Alert("All fields are required", "error")
		}   

    }	


	function emailExist(e) {

		e.preventDefault();

		if(email !== upEmail) {

		const options = {
            method: 'POST',
            headers: { 
            			'Content-Type': 'application/json',
            			Authorization:  `Bearer ${ AppHelper.getAccessToken() }`
        			},
            body:JSON.stringify({
                email: upEmail	                  

            })
        }       

    	fetch(`${AppHelper.API_URL}/users/email-exists`, options)
        .then(AppHelper.toJSON)
        .then(res => {

        	if(res) {
        		Alert("Email already exist, input another one!", "error")
        		console.log(res)
        		
        	}else {
        		updateProfile()
        	}

	    })  

    	}else {

    		updateProfile()
    	}

	}

	function deleteAccount() {

		const options = {
			method: 'DELETE',
            headers: { Authorization:  `Bearer ${ token }`}
        }

        fetch(`${ AppHelper.API_URL}/users/${id}`, options)
        .then(AppHelper.toJSON)
        .then(res=> {

        	if(res) {
        		Alert("Your account has been successfully deleted!", "success")
        		handleClose()
            	reloadData()
            	localStorage.removeItem('token')
     			Router.push('/login')
        	}else {

        		Alert('Error found! Please try again', 'error')
        	}
        		
        })	
        	
	}
	

	useEffect(() => {

		if(upEmail !== email || upFirstName  !== firstName || upLastName  !== lastName) {

			setIsActive(true);
		}else {

			setIsActive(false);
		}

	}, [show, upEmail, upFirstName, upLastName])



    useEffect(() => {
		setToken(localStorage.getItem('token'))					
	}, [])

	useEffect(() => {

		
        const options = {
            headers: { Authorization:  `Bearer ${ token }`}
        }

        fetch(`${ AppHelper.API_URL}/users/details`, options)
        .then(AppHelper.toJSON)
        .then(data=> {

        	setId(`${data._id}`)           
        	setName(`${data.firstName} ${data.lastName}`)
        	setFirstName(data.firstName)
        	setLastName(data.lastName)            	
        	setEmail(data.email)
        	setUpFirstName(data.firstName)
        	setUpLastName(data.lastName)            	
        	setUpEmail(data.email)

        	if(data.categories !== undefined && data.transactions !== undefined) {
        		setCategoriesCount(data.categories.length)
				setTransactionsCount(data.transactions.length) 
        	}			
        	
 			// console.log(data)
    		
        })			

	}, [token, reload])	

	return(
		<React.Fragment>
			{email !== undefined
				?				
					<Row className="justify-content-center">
					<Card style={{ width: '25rem' }} className="px-2 bg-light">
						<h1 className="display-1 text-center text-secondary"><FaIcons.FaUserCircle /></h1>				
						<Card.Body className="bg-light">
						    <Card.Title className="text-center">{name}</Card.Title>
						    <Card.Text className="text-center pt-0">
						      {email}
						    </Card.Text>
						</Card.Body>
						<Row className="my-5">
							<Col xs md={6} className="">
								<Card border="light" className="bg-light text-info">					   
								    <Card.Body className="text-center">					      
								      <Card.Text>
								        {categoriesCount}							        
								      </Card.Text>
								      <small className="text-muted">Categories</small>
								    </Card.Body>
								</Card>
							</Col>
							<Col xs md={6} className="">
								<Card border="light" className="bg-light text-info">					   
								    <Card.Body className="text-center">					      
								      <Card.Text>
								        {transactionsCount}							        
								      </Card.Text>
								      <small className="text-muted">Transactions</small>
								    </Card.Body>
								</Card>
							</Col>
						</Row>			
							<Card.Body className="text-center">
							    <Card.Link href="#" role="button" onClick={handleShow} className="text-info">Edit Profile</Card.Link>
							    <Card.Link href="#" role="button" onClick={handleShowConfirm} className="text-danger">Delete Account</Card.Link>
							    <Card.Link href="/logout" className="text-info">Log out</Card.Link>
							</Card.Body>
						</Card>
					</Row>

				:
					
					<Loader />
						
			}

			{token ? <AddTransaction  reload={reloadData} /> : <Loader /> }

			<Modal show={show} onHide={handleClose}>		        
		          <Modal.Title className="my-5 text-center">Edit Profile</Modal.Title>		        
		        <Modal.Body>
		        				    		    
					    	<Form onSubmit={(e) => emailExist(e)}>
								<Form.Group controlId="firstName">							
									<Form.Control
										type="text"
										placeholder="Enter your first name"
										value={upFirstName}
										onChange={(e) => setUpFirstName(e.target.value)}
										required
									/>				
								</Form.Group>
								<Form.Group controlId="lastName">							
									<Form.Control
										type="text"
										placeholder="Enter your last name"
										value={upLastName}
										onChange={(e) => setUpLastName(e.target.value)}
										required
									/>				
								</Form.Group>
								<Form.Group controlId="userEmail">							
									<Form.Control
										type="email"
										placeholder="Enter email"
										value={upEmail}
										onChange={(e) => setUpEmail(e.target.value)}
										required
									/>									
								</Form.Group>								
									<Button variant="secondary" type="button" className="my-3" block onClick={handleClose}>
										Cancel
									</Button>
								{isActive ?

										<Button variant="info" type="submit" id="submitBtn" className="my-3" block >
											Save
										</Button>
									:
										<Button variant="info" type="submit" id="submitBtn" className="my-3" block disabled>
											Save
										</Button>
								}								                               						
							</Form>			
		        </Modal.Body>
		    </Modal>
		    <Modal show={showConfirm} onHide={handleCloseConfirm} animation={false}> 
		   
		        <Modal.Body>You are about to delete your account. Do you want to proceed?</Modal.Body>
		        <Modal.Footer>
		          <Button variant="secondary" onClick={handleCloseConfirm}>
		            Cancel
		          </Button>
		          <Button variant="info" onClick={deleteAccount}>
		            Confirm
		          </Button>
		        </Modal.Footer>
      		</Modal>			
		</React.Fragment>
	)

}

