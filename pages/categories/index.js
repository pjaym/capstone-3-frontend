import React, { useState, useEffect, useContext } from 'react';
import { Row, Col } from 'react-bootstrap';
import Router from 'next/router';
import Swal from 'sweetalert2';
import * as FaIcons from 'react-icons/fa';
import Category from  '../../components/Category';
import AddCategory from  '../../components/AddCategory';
import AppHelper from '../../helper/app-helper';
import ErrorAlert from '../../helper/errorAlert';
import UserContext from  '../../UserContext';


export default function index() {

	const { user } = useContext(UserContext);

	const [token, setToken] = useState(user.token);
	const [userData, setUserData] = useState({});
	const [userCategory, setUserCategory] = useState([]);
	const [categories, setCategories] = useState('');
	const [reload, setReload] = useState(false);

	// Function to reload/refresh data upon update
	function reloadData() {
		
		reload ? setReload(false) : setReload(true);
		console.log("paging reload 1")
	}

	// Get token from local storage
	useEffect(() => {
		setToken(localStorage.getItem('token'))
	}, [])

	//  Get user details
	useEffect(() => {

		console.log(token)
		console.log("paging reload 2")
		
		const options = {
            headers: { Authorization:  `Bearer ${ token }`}
        }

        fetch(`${AppHelper.API_URL}/users/details`, options)
			        .then(AppHelper.toJSON)
			        .then(data => {

			        // console.log(data);
			        setUserData(data);

						
		})		

	}, [token, reload])


	// Get categories of the user
	useEffect(() => {		

		if (userData.categories !== undefined) {

			let tempUserCategories = [];

			userData.categories.map((userCategory, index) => {

				fetch(`${AppHelper.API_URL}/categories/${userCategory.categoryId}`)
		        .then(AppHelper.toJSON)
		        .then(category => {
			  
	    			tempUserCategories.push(category);	

	    			if(tempUserCategories.length == userData.categories.length) {
						console.log(tempUserCategories.length);
						setUserCategory(tempUserCategories);
					}			

			    })
		        
			})
			// console.log(tempUserCategories);			
		}
		else {

		}		

	}, [userData])


	// Render the categories
	useEffect(() => {
		
		if (userCategory.length > 0) {

			console.log("pwede na  magrender")			
			// console.log(userCategory)

			setCategories(userCategory.map((category) => {

				if(!category.isDeleted) {
					// console.log(category)
					return(												
						<Category key={category._id} category={category} />						
					)

				}else {
					return null;
				}
			
			}))

		}else {

			// ErrorAlert();
			//Router.push('/login');
		}
		

	}, [userCategory])

	console.log(categories)

	return (
		<React.Fragment>
			{userCategory.length > 0 
				? 
					<React.Fragment>
						<Row>
							{categories}
						</Row>						
					</React.Fragment>
				:
					<React.Fragment>
						<h1 className="display-1 text-center text-secondary mt-5"> <FaIcons.FaGrinBeamSweat/> </h1>
						<h3 className="display-5 text-center text-secondary"> There are no categories yet.</h3>
						<h3 className="display-5 text-center text-secondary"> Tap the + button to add your first category. </h3>
					</React.Fragment>

			}
			{token 	? <AddCategory reload={reloadData}/>
					: null
			}									
		</React.Fragment>		
	)
}




			
			
			

						        

			        
		
			        
			    								
			

							
		
