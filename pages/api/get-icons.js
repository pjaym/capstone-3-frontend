import fs from 'fs';
import path from 'path';

export default function handler (req, res) {
  const dirRelativeToPublicFolder = 'icons'

  const dir = path.resolve('./public', dirRelativeToPublicFolder);

  const filenames = fs.readdirSync(dir);

  const icons = filenames.map(name => path.join('/', dirRelativeToPublicFolder, name))

  res.statusCode = 200
  res.json(icons);

}