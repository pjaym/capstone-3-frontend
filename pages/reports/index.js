import React, { useState, useEffect, useContext } from 'react';
import { Row, Col, Form, Container } from 'react-bootstrap';
import * as FaIcons from 'react-icons/fa';
import AppHelper from '../../helper/app-helper';
import UserContext from  '../../UserContext';
import BalanceTrend from '../../components/BalanceTrend';
import CategoryBreakdown from '../../components/CategoryBreakdown';
import BottomNav from '../../components/BottomNav';
import AddTransaction from '../../components/AddTransaction';

export default function index() {

	const { user } = useContext(UserContext);

	const [token, setToken] = useState(user.token);
	const [userData, setUserData] = useState({});
	const [userTransaction, setUserTransaction] = useState([]);
	const [transactions, setTransactions] = useState('');
	const [categories, setCategories] = useState('');
	const [reload, setReload] = useState(false);
	const [searchAndFilter, setSearchAndFilter] = useState('');
	const [filteredTransaction, setFilteredTransaction] = useState(userTransaction);
	const [balanceTrend, setBalanceTrend] = useState([]);
	const [categoryBreakdown, setCategoryBreakdown] = useState([]);


	function reloadData() {		
		
		console.log("paging reload delete transaction")
	}

	// Get token from local storage
	useEffect(() => {
		setToken(localStorage.getItem('token'))
	}, [])

	//  Get user details
	useEffect(() => {
		
		const options = {
            headers: { Authorization:  `Bearer ${ token }`}
        }

        fetch(`${AppHelper.API_URL}/users/details`, options)
			        .then(AppHelper.toJSON)
			        .then(data => {

			        // console.log(data);
			        setUserData(data);
		})


	}, [token, reload])


	// Get transactions of the user
	useEffect(() => {		

		if (userData.transactions !== undefined) {

			let tempUserTransactions = [];

			userData.transactions.map((userTransaction, index) => {

				fetch(`${AppHelper.API_URL}/transactions/${userTransaction.transactionId}`)
		        .then(AppHelper.toJSON)
		        .then(transaction => {

		        	fetch(`${AppHelper.API_URL}/categories/${transaction.categoryId}`)
			        .then(AppHelper.toJSON)
			        .then(category => {

				        transaction.name = category.name;
		    			transaction.categoryIcon = category.iconPath;

		    			if(transaction.type === "expense") {

			        		transaction.amountR = 0 - transaction.amount
			        		
		        		}else  {

		        			transaction.amountR = transaction.amount
		        		}

		    			tempUserTransactions.push(transaction);	

		    			if(tempUserTransactions.length == userData.transactions.length) {

		    				tempUserTransactions.sort(function(a,b){
							  return new Date(a.transactionDate).getTime() - new Date(b.transactionDate).getTime();
							})
		    				/*console.log(transaction.amount)
							console.log(tempUserTransactions);*/
							setUserTransaction(tempUserTransactions);
							setFilteredTransaction(tempUserTransactions);
						}				       
					})

			    })
		        
			})
			// console.log(tempUserTransactions);		
		}		

	}, [userData])

	useEffect(() => {

		if(userTransaction !== undefined && userTransaction.length > 0) {
		
			setBalanceTrend(() => <BalanceTrend reportData={userTransaction} />);
			setCategoryBreakdown(() => <CategoryBreakdown reportData={userTransaction} />);	
		}

	}, [userTransaction])
	
	// console.log(userTransaction.length)

	return (
		<React.Fragment>
			
			{userTransaction.length > 0 && userTransaction !== undefined
				? 	
					<React.Fragment>	
						{balanceTrend}						
					</React.Fragment>

				: 	<React.Fragment>	
						<h1 className="display-1 text-center text-secondary mt-5"> <FaIcons.FaChartArea/> </h1>
						<h3 className="display-5 text-center text-secondary"> Nothing here yet.</h3>
						<h5 className="display-5 text-center text-secondary"> Come back once you've added a few entries </h5>
					</React.Fragment>
			}
			
			{token ? <AddTransaction  reload={reloadData} /> : null}						
		</React.Fragment>
	)
}
