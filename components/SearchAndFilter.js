import React, { useState, useEffect } from 'react';
import { Form, Col, Container, Row } from  'react-bootstrap';
import * as FaIcons from 'react-icons/fa';

export default function SearchAndFilter({data, searchAndFilter, show, reload}) {
	
	// console.log(data);

	const [result, setResult] = useState(data)
	const [searchData,  setSearchData] = useState('')
	const [filterData,  setFilterData] = useState('')

	useEffect(() => {
		

		if ((searchData.length > 0 || filterData !== "Type")) {			

			//let searchRegex = new RegExp(searchData, "i")
			// console.log(data)
			const searchResult = data.filter(data => {

				if (searchData.length === 0 && filterData !== "Type") {

					return (!data.isDeleted && data.type === filterData.toLowerCase());

				}else if (searchData.length > 2 && filterData === "Type" || filterData === "") {

					return ((data.name.toLowerCase().includes(searchData.toLowerCase()) || data.notes.toLowerCase().includes(searchData.toLowerCase())) && (!data.isDeleted) );
				
				}else if (searchData.length > 2 && filterData !== "Type") {

					return ((data.name.toLowerCase().includes(searchData.toLowerCase()) || data.notes.toLowerCase().includes(searchData.toLowerCase())) && (!data.isDeleted) && (data.type.toLowerCase() === filterData.toLowerCase()));

				} 
			})
			
			setResult(searchResult)
			
		}

		if (searchData.length === 0 && (filterData === "Type" || filterData === "")) {
			reload()
			// setResult(data)
			// console.log('result == data')
		}
		
	
	// console.log(`FilterData: ${filterData}`)
	// console.log(`Search Data: ${searchData}`)

	}, [searchData, filterData])
	

	useEffect(() => {
		// console.log(`Result: ${result}`)
		searchAndFilter(result)
	}, [result])	

	return(
		<React.Fragment>
			<Container fluid className="justify-content-center py-3">
				<Row>
					<Col md={9} xs={12}>	
						<Form.Group>
						    <Form.Control 
						    	type="text" 
						    	placeholder="Search" 
						    	className=" mr-sm-2"
						    	value={searchData}
						    	onChange={(e) => setSearchData(e.target.value)}
						    />
						</Form.Group>
					</Col>				
					<Col md={3} xs={12}>		
						<Form.Group as={Col} controlId="formGridState">
					      	<Form.Control 
					      		as="select"
					      		value={filterData}
					      		onChange={(e) => setFilterData(e.target.value)}
					      	>
						        <option>Type</option>
						        <option>Income</option>
						        <option>Expense</option>
					      	</Form.Control>
					    </Form.Group>
				    </Col>
			    </Row>
		    </Container>
	    </React.Fragment>	
	)
}