import React, { useState, useEffect, useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Tab, Row, Col } from 'react-bootstrap';
import Link from 'next/link';
// import { withRouter } from 'next/router';
import * as FaIcons from 'react-icons/fa';
import * as IoIcons from 'react-icons/io';
import * as GiIcons from 'react-icons/gi';
import UserContext from '../UserContext';
import AppHelper from '../helper/app-helper';


export default function NavBar({ router }) {

	const { user } = useContext(UserContext);
	const [token, setToken] = useState(user.token);
  	const [isLoggedIn, setIsLoggedIn] = useState(false);
  	const [userData, setUserData] = useState(false);


  	function getToken() {
  		setToken(localStorage.getItem('token'))

  		if(token !== null) {
  			setIsLoggedIn(true)  			 
  		}else {
  			setIsLoggedIn(false)
  		}  		
  	} 

  	function getUserDetails() {
		
		console.log(UserData(token))		
	} 	

	useEffect(() => {   
	    if(token !== null) {
	      setIsLoggedIn(true)	      
	    }else {
	      setToken(localStorage.getItem('token'))
	      
	    }
	}, [])


	useEffect(() => {
	    window.addEventListener("beforeunload", getToken());
	    return () => {
	      window.removeEventListener("beforeunload", getToken());
	    };
	    	   
	}, [token]);


	useEffect(() => {
		const options = {
	        headers: { Authorization:  `Bearer ${ token }`}
	    }

	 	 fetch(`${AppHelper.API_URL}/users/details`, options)
			        .then(AppHelper.toJSON)
			        .then(data => {

			        // console.log(data);
			        setUserData(data);			        
		})

	}, [token])

	

	return(
		<React.Fragment>
			<Navbar bg="info" expand="lg" className="fixed-top w-100">
							
				<Link href="/">
			  		<h5 className="px-4 my-3 text-white" onClick={getToken}><GiIcons.GiWallet /> SpendRite</h5>
			  	</Link>	
			  	{isLoggedIn || token !== null ?				
					<Nav className="ml-auto">					
				      	<Link href="/logout">
					  		<h3 className="nav-brand text-white" role="button"><span className="text-dark"> Hi {userData.firstName}!</span> <IoIcons.IoIosExit /></h3>
					  	</Link>
				    </Nav>				    	
				: null
				}
			</Navbar>			
		</React.Fragment>
	)
}
 
 // <small>Transactions</small>	