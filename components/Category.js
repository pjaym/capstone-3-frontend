import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import { Card, Button, Row, Col } from 'react-bootstrap';
import path from 'path';
import Image from 'next/image';




export default function Category({category}) {

    const [categoryName, setCategoryName] = useState(0);
    const [categoryArr, setCategoryArr] = useState([])

    const { name, type, createdOn, iconPath } = category;    


    return (
            <Col md={3} lg={3} xl={3}>
                <Card className={type === 'income' ? "my-3 text-left bg-light border-top-0 border-bottom-0 border-right-0 border-left-3 rounded-0 border-info" : "my-3 text-left bg-light border-top-0 border-bottom-0 border-right-0 rounded-0 border-danger border-5"}>        
                    <Card.Body className="text-left">
                        <Row>
	                        <Col md={5} xs={5} lg={3} className="float-right">                       
		                        <Image
		                            src={path.resolve('./public', iconPath)}
		                            alt="Icon"
		                            width={45}
		                            height={45}
		                        />                        
	                        </Col>
	                        <Col md={7} xs={7}>
	                        	<Card.Title className="float-left">{name}</Card.Title>

	                        </Col>
                            <small className={type === 'income' ? 'text-info' : 'text-danger'}>{type.charAt(0).toUpperCase() + type.slice(1)}</small>             
                        </Row>
                    </Card.Body>
                </Card>
            </Col>
    )
}

