import React, { useState, useEffect } from 'react';
import Spinner from 'react-bootstrap/Spinner';
import { Row, Col, Container } from  'react-bootstrap';
import Modal from 'react-bootstrap/Modal';
import Image from 'next/image';

export default function CustomLoadingScreen() {

	return (
		<React.Fragment>
			<div
		        style={{
		        	display: "flex",
          			justifyContent: "center",          			
          			height: "80vh",          			
          			alignItems: "center",				 
		        }}
		    >                                
	            <Image
			            src="/loader.gif"
			            alt="Icon"
			            width={200}
			            height={200}
		      	/>
            </div>      		
  		</React.Fragment>

	)
}


{/*<Spinner animation="grow" size="sm" />
<Spinner animation="grow" size="md" />
<Spinner animation="grow" />*/}

// display: "flex",
// 		          justifyContent: "center",
// 		          margin: "auto",
// 		          height: "50%",
// top: "50%",