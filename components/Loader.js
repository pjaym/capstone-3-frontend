import React, { useState, useEffect } from 'react';
import Image from 'next/image';

export default function Loader() {

	return (

		<React.Fragment>
			<div
		        style={{
		        	display: "flex",
          			justifyContent: "center",          			
          			height: "80vh",          			
          			alignItems: "center",				 
		        }}
		    >                                
	            <Image
			            src="/loader.gif"
			            alt="Icon"
			            width={200}
			            height={200}
		      	/>
            </div>      		
  		</React.Fragment>

	)
}