import React, { useState, useEffect } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Tab, Row, Col, Container, Button } from 'react-bootstrap';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { BrowserView, MobileView, isBrowser, isMobile } from "react-device-detect";
import * as FaIcons from 'react-icons/fa';
import * as IoIcons from 'react-icons/io';
import * as GiIcons from 'react-icons/gi';
import AppHelper from '../helper/app-helper';
import UserContext from '../UserContext';



export default function NavBar({ relod }) {

	const [router, setRouter] = useState(useRouter());

	function reload() {

		reload()
	}

	return(
		<React.Fragment>
			<Container className="mt-5">
				<Row>
					<Col md={9} xs={12}>
					<Row>				
					<Navbar bg="light" className="fixed-bottom">
						<Col md={3} xs={2} className="text-center">
							<Link href="/transactions">
						  		<h3 role="button" className={router.pathname === "/transactions" ? "text-info" : "text-secondary"}><IoIcons.IoMdWallet /></h3>			  		
						  	</Link>
						  	<BrowserView>
						  		<small className="text-center">Transactions</small>
						  	</BrowserView>
					  	</Col>
					  	<Col md={3} xs={2} className="text-center">
							<Link href="/reports">
								<h3 role="button" className={router.pathname === "/reports" ? "text-info" : "text-secondary"}><FaIcons.FaChartPie /></h3>
							</Link>
							<BrowserView>
								<small className="text-center">Reports</small>
							</BrowserView>
						</Col>
						<Col md={3} xs={2} className="text-center">	
						    <Link href="/categories">
							  	<h3 role="button" className={router.pathname === "/categories" ? "text-info" : "text-secondary"}><FaIcons.FaDelicious /></h3>
							</Link>
							<BrowserView>
								<small className="text-center">Categories</small>
							</BrowserView>
						</Col>
						<Col md={3} xs={2} className="text-center">							
							<Link href="/profile">
							  	<h3 role="button" className={router.pathname === "/profile" ? "text-info" : "text-secondary"}> <FaIcons.FaUser /> </h3>
							</Link>
							<BrowserView>
								<small className="text-center">Profile</small>
							</BrowserView>							
						</Col>				    	  
					</Navbar>
					</Row>
					</Col>
				</Row>
			</Container>			
		</React.Fragment>
	)
}
 
 // <small>Transactions</small>	