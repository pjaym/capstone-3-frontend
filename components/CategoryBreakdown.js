import React,  { useState, useEffect } from 'react';
import { Pie } from 'react-chartjs-2';
import { Container, Col, Card } from 'react-bootstrap';
import DateFilter from  '../components/DateFilter';


export default function	CategoryBreakdown({reportData, startDate,  endDate}) {


	const [incomeTotal, setIncomeTotal] = useState([]);
	const [expenseTotal, setExpenseTotal] = useState([]);
	const [incomeData, setIncomeData] = useState([]);
	const [expenseData, setExpenseData] = useState([]);


	useEffect(() => {


		
	}, [startDate, endDate])

	// console.log(`${startDate} ${endDate}`)


	// Create an income and expense array
	useEffect(() => {

		if (reportData.length > 0) {	
		
			let tempIncome = []; 
			let tempExpense = [];

			reportData.forEach((transaction) => {
				// console.log(transaction)

				if(!tempExpense.includes(transaction.name) && !transaction.isDeleted && transaction.type == 'expense' && new Date(transaction.transactionDate).getTime() >= startDate && new Date(transaction.transactionDate).getTime() <= endDate) {

					tempExpense.push(transaction.name)

				}else if(!tempIncome.includes(transaction.name) && !transaction.isDeleted && transaction.type == 'income' && new Date(transaction.transactionDate).getTime() >= startDate && new Date(transaction.transactionDate).getTime() <= endDate) {

					tempIncome.push(transaction.name)
				}
			})

			setIncomeData(tempIncome)
			setExpenseData(tempExpense)
			// console.log(`Income: ${tempIncome}`)
			// console.log(`Expense: ${tempExpense}`)

		}

	}, [startDate, endDate])


	// Get the total amount per income category
	useEffect(() => {

		if (reportData.length > 0 && startDate !== undefined && endDate !== undefined) {

			setIncomeTotal(incomeData.map((category) => {

				// console.log(category)

				let total = 0;

				reportData.forEach((transaction) => {

					if(category === transaction.name && !transaction.isDeleted && new Date(transaction.transactionDate).getTime() >= startDate && new Date(transaction.transactionDate).getTime() <= endDate) {

						total += transaction.amount
					}
				})

	  			return total;
			}))

		}
		
	}, [incomeData])

	// console.log(`incomeTotal: ${incomeTotal}`)
	
	// Get the total amount per expense category
	useEffect(() => {

		if (reportData.length > 0 && startDate !== undefined && endDate !== undefined) {

			setExpenseTotal(expenseData.map((category) => {

				// console.log(category)

				let total = 0;

				reportData.forEach((transaction) => {

					if(category === transaction.name && !transaction.isDeleted && new Date(transaction.transactionDate).getTime() >= startDate && new Date(transaction.transactionDate).getTime() <= endDate) {

						total += transaction.amount
					}
				})

	  			return total;
			}))

		}
		
	}, [expenseData])

	// console.log(`expenseTotal: ${expenseTotal}`)
	
	
	/*const labels = balanceTrend;
	const data = {
	  labels: labels,
	  datasets: [{
	    label: 'Balance Trend',
	    data: balanceTrend,
	    fill: true,
	    borderColor: 'rgb(75, 192, 192)',
	    width: 50,
  		height: 50  
	   
	  }]
	// };*/
	// rgb(3, 152, 158) 0%, rgb(3, 152, 158) 100%)

	const incData = {
	  labels: incomeData,
	  datasets: [{
	    label: 'My First Dataset',
	    data: incomeTotal,
	    backgroundColor: [
	      'rgb(3, 152, 158)',	      
	      'rgb(222, 218, 251)',
	      'rgb(148, 148, 148)',
	      'rgb(104, 173, 202)'
	    ],
	    hoverOffset: 4
	  }]
	};

	const expData = {
	  labels: expenseData,
	  datasets: [{
	    label: 'My First Dataset',
	    data: expenseTotal,
	    backgroundColor: [
	      'rgb(255, 87, 87)',
	      'rgb(255, 98, 130)',	      
	      'rgb(167, 172, 181)'
	    ],
	    hoverOffset: 4
	  }]
	};


	return (
		<React.Fragment>
				<Col md={4}>
					<Card className="py-2">			    
					    <Card.Body>
					      <Card.Title>Income Breakdown</Card.Title>
					      	<Pie data={incData} />
					    </Card.Body>			  
					 </Card>
				 </Col>
				 <Col md={4}>
					 <Card className="py-2">			    
					    <Card.Body>
					      <Card.Title>Expenses Breakdown</Card.Title>
					      	<Pie data={expData} />
					    </Card.Body>			  
					 </Card>				
				 </Col>
		</React.Fragment>
	)
}

/*<DateFilter dateFilter={dateFilter} />
			{balanceTrend  
				? <h5><Line data={data} /></h5>					
				: "Loading.."
			} */
