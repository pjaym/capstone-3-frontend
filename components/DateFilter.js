import React,  { useState, useEffect } from 'react';
import { Line } from 'react-chartjs-2';
import { Container, Col, Row, Form } from 'react-bootstrap';
import Moment from 'moment';
import SelectCategory from  '../components/SelectCategory';


export default function	DateFilters({dateFilter}) {


	const [startDate, setStartDate] = useState(Moment(new Date()).subtract(31, "days").format("yyyy-MM-DD"));
	const [endDate, setEndDate] = useState(Moment(new Date()).format("yyyy-MM-DD"));


	// Pass the date range to balance trend
	useEffect(() => {

		dateFilter(startDate + "T00:00:00+08:00" , endDate + "T23:59:59+08:00")	

	}, [startDate, endDate])


	return (
		<React.Fragment>
			<Container fluid>
				<Form>
					<Form.Row>
						<Col md={6} className="ml-0 p">
					    	<Form.Group className="">
								<Form.Label>From</Form.Label>
								<Form.Control
									type="date"
									placeholder="From"
									size="lg"
									value={startDate}
									onChange={(e) => setStartDate(e.target.value)}
									required
								/>
							</Form.Group>
						</Col>
						<Col md={6}>
							<Form.Group className="">
								<Form.Label>To</Form.Label>
								<Form.Control
									type="date"
									placeholder="To"
									size="lg"
									value={endDate}
									onChange={(e) => setEndDate(e.target.value)}
									required
								/>
							</Form.Group>
						</Col>
				    </Form.Row>
			    </Form>	
			</Container>
		</React.Fragment>
	)
}