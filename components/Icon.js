import React, { useState, useEffect } from 'react';
import { Row, Col, Button, Form, ButtonGroup, Container, ToggleButton } from 'react-bootstrap';
import Dropdown from 'react-bootstrap/Dropdown';
import Modal from 'react-bootstrap/Modal';
import path from  'path';
import Image from 'next/image';


export default function Test({iconPath, get}) { 

	const [show, setShow] = useState(false);
	const [ico, setIco] = useState('');
	const [iconURI, setIconURI] = useState('');


	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	useEffect(() => {
		setIconURI(iconPath.path.replace(/\\/g, '/'));
	}, [])	
	

	useEffect(() => {

		if(ico !== '') {			
			get(ico)
		}	

	}, [ico])


	return(
		
		<ToggleButton variant="white" type="radio" name="radio" key={iconURI} value={iconURI} onChange={(e) => setIco(e.currentTarget.value)} >     
	    	<Image
	            src={path.resolve('./public', iconURI)}
	            alt="Icon"
	            width={40}
	            height={40}
		    />
		</ToggleButton>	
	                  
	)
}

