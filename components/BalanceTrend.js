import React,  { useState, useEffect } from 'react';
import { Line } from 'react-chartjs-2';
import { Container, Row, Col, Card } from 'react-bootstrap';
import DateFilter from  '../components/DateFilter';
import CategoryBreakdown from  '../components/CategoryBreakdown';
import TransactionDashboard from  '../components/TransactionDashboard';

export default function	BalanceTrend({reportData}) {


	const [balanceTrend, setBalanceTrend] = useState([]);
	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');
	const [categoryBreakdown, setCategoryBreakdown] = useState('');


	function dateFilter(start, end) {

		setStartDate(new Date(start).getTime())
		setEndDate(new Date(end).getTime())
		
	}
	console.log(reportData)

	useEffect(() => {

		if(startDate !== undefined && endDate !== undefined) {
			setCategoryBreakdown(() => <CategoryBreakdown startDate={startDate} endDate={endDate} reportData={reportData}/>);
		}		
		
	}, [startDate, endDate])

	// console.log(`${startDate} ${endDate}`)
	// Balance Trend Computation
	useEffect(() => {		
		

		if (reportData.length > 0 && startDate !== undefined && endDate !== undefined) {			
		
					    
		    let tempbal = [];
		    let count = 0;

		    const firstIndex = reportData.findIndex((transaction) => (!transaction.isDeleted ? new Date(transaction.transactionDate).getTime() >= startDate : null))

		    console.log(firstIndex)

		    reportData.forEach((transaction, index) => {		    	

		    	if (!transaction.isDeleted && new Date(transaction.transactionDate).getTime() >= startDate && new Date(transaction.transactionDate).getTime() <= endDate) {
		    		
		    			// transaction.amount = (transaction.type === "expense") ? (-transaction.amount) : (transaction.amount);

		    			// console.log(`${transaction.type} ${transaction.amountR} `)

			    		if(index === firstIndex) {			    			

			    			tempbal.push(transaction.amountR)

			    		}else {			    			

			    			tempbal.push(tempbal[count] + transaction.amountR)
			    			count++;

			    			// console.log(new Date(transaction.transactionDate).getTime())
			    			// console.log(transaction.amountR)
			    		}
		    		
		    	} 
		    	// console.log(new Date(transaction.transactionDate).getTime())
		    })

      	
      		// const balanceArr = 
			setBalanceTrend(tempbal)
		  	

		}else {
			return null;
		}


	}, [startDate, endDate])


	const labels = balanceTrend;
	const data = {
	  labels: labels,
	  datasets: [{
	    label: 'Balance Trend',
	    data: balanceTrend,
	    fill: true,
	    borderColor: 'rgb(75, 192, 192)',
	    width: 50,
  		height: 50  
	   
	  }]
	};


	return (
		<React.Fragment>
			<Container fluid>
			<DateFilter dateFilter={dateFilter} />
			<TransactionDashboard userTransaction={reportData} startDate={startDate} endDate={endDate}/>
			<Row>
				<Col md={4}>					
					<Card className="py-2">			    
					    <Card.Body>
					      <Card.Title>Balance Trend</Card.Title>
					      	{balanceTrend  
								? <h5><Line data={data} /></h5>					
								:  	null 
							}
					    </Card.Body>			  
					 </Card>					 
				 </Col>
				 {categoryBreakdown}
			 </Row>		
			</Container>
		</React.Fragment>
	)
}


/*setBalanceTrend(reportData.reduce((acc,transaction, index) => { 
        		
        		if (!transaction.isDeleted) {
        			
        			transaction.amount = transaction.type === "expense" ? 0 - transaction.amount : transaction.amount

					// console.log(`${transaction.type} ${transaction.amount}`)
				

			        if (index === 0) { 
			        	acc.push(transaction.amount); 
			       
			       	}else {
			          
			          acc.push(acc[index-1] + transaction.amount);
			        
			        }	        			
        		}
		                
		        return acc;


		    } ,[]));*/