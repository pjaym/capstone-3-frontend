import React, { useState, useEffect, useContext } from 'react';
import Router from 'next/router'
import { Row, Col, Button, ButtonGroup, Form, Container, Navbar } from 'react-bootstrap';
import { useRouter } from 'next/router';
import Modal from 'react-bootstrap/Modal';
import Image from 'next/image';
import Link from 'next/link';
import path from  'path';
import Moment from 'moment';
import { BrowserView } from "react-device-detect";
import * as IoIcons from 'react-icons/io';
import * as FaIcons from 'react-icons/fa';
import Select from 'react-select';
import AppHelper from '../helper/app-helper';
import Alert from '../helper/alert';
import SelectCategory from  '../components/SelectCategory';
import UserContext from  '../UserContext';





export default function AddTransaction({ reload }) {

	const { user } = useContext(UserContext);

	const [type, setType] = useState('type'.toLowerCase());
	const [path, setPath] = useState('');
	const [amount, setAmount] = useState('');
	const [date, setDate] = useState(Moment(new Date()).format("yyyy-MM-DD"));
	const [notes, setNotes] = useState('');
	const [isActive, setIsActive] = useState(false);
	const [disabled, setDisabled] = useState(true);
	const [userData, setUserData] = useState({});
	const [userCategory, setUserCategory] = useState([]);
	const [categoryId, setCategoryId] = useState('');
	const [filteredCategories, setfilteredCategories] = useState([]);
	const [router, setRouter] = useState(useRouter());
	const [msg, setMsg] = useState('No available categories');

	// const [reload, setReload] = useState(false);
	const [token, setToken] = useState(user.token);

	// const {name} = categories
	// console.log(Moment(new Date()).format("YYYY/MM/DD"))

	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	

	async function selectCategory() {					

		if (userData.categories !== undefined) {

			let tempUserCategories = [];
			
			userData.categories.map((userCategory, index) => {

				fetch(`${AppHelper.API_URL}/categories/${userCategory.categoryId}`)
		        .then(AppHelper.toJSON)
		        .then(category => {
			  		
		        	category.label = category.name;
		    		category.value = category._id; 
		    		// console.log(type)
			  		
			  			tempUserCategories.push(category);
			  						  		   			   			

	    			if(tempUserCategories.length == userData.categories.length) {
						// console.log(tempUserCategories.length);
						setUserCategory(tempUserCategories);
						setfilteredCategories(userCategory);

					}			

			    })
		        
			})

		}			
	}

	const customStyles = {
	  option: (provided, state) => ({
		    ...provided,
		    backgroundColor: state.isSelected ? '#17a2b8' : 'white',
		    color: state.isSelected ? 'white' : 'gray',
		})
	}


    // Add a transaction
    function addTransaction(e) {
    	
    	e.preventDefault();
    	
    	let tempDate = Moment(new Date()).format()
		let tempTime = tempDate.substr(10, tempDate.length)

		const options = {
            method: 'POST',
            headers: { 
            	'Content-Type': 'application/json',
            	'Authorization': `Bearer ${AppHelper.getAccessToken()}`
            },
            body:JSON.stringify({
                amount: amount,
                type: type.toLowerCase(),
                categoryId: categoryId,
                transactionDate: date + tempTime,
                notes: notes

            })
        }

        // console.log(options)

		fetch(`${AppHelper.API_URL}/transactions`, options)
        .then(AppHelper.toJSON)
        .then(data => {
            if(data) {
            	console.log("Transaction added successfully")
            	Alert("Transaction added successfully", "success")
            	console.log(data)
            	reload()

            }else {
            	Alert("Error found! Please try again", "error")
            	console.log("Error found")
            }
        })

  		handleClose()   
    }

    // Clear the form when the modal is close
    useEffect(() => {

    	if (!show) {    		
    		setCategoryId('')
	        setType('')
	        setAmount('')		
			setNotes('')
			setDate(Moment(new Date()).format("yyyy-MM-DD"))				
    	} 
    	setDisabled(true)  	
    	
    }, [show])

    //  Get user details
	useEffect(() => {

		// console.log(token)		
		const options = {
            headers: { Authorization:  `Bearer ${ AppHelper.getAccessToken() }`}
        }

        fetch(`${AppHelper.API_URL}/users/details`, options)
			        .then(AppHelper.toJSON)
			        .then(data => {

			        // console.log(data);
			        setUserData(data);
		})

	}, [])

	useEffect(() => {
		
		if(amount !== '' && type !== '' && type !== 'Type') {
			setIsActive(true);
		}else {
			setIsActive(false);
		}
	
	}, [type, amount, categoryId])

	// Filter the categories by selected type
	useEffect(() => {
		
		//selectCategory()
		// console.log(type.toLowerCase())
		// console.log(filteredCategories)
		

		if (type !== 'Type' && userCategory.length > 0) {

			setDisabled(false)

			setfilteredCategories(userCategory.filter(category => {
				return category.type === type.toLowerCase();
				console.log(category)
				
			}))
			console.log(filteredCategories)

		}else {
			setDisabled(true)
		}

	}, [type])

	useEffect(() => {
		if (type === "Type" || type === "type" || type === '') {
			setMsg("No available categories")
		}else {			
			setMsg("Loading...")
		}	
		console.log(type)	
	}, [filteredCategories, type])
	

  return (  	
    <React.Fragment>
   		<Container className="w-50">
			<Row>
				<Col md={9} xs={12}>
					<Row>				
						<Navbar bg="light" expand="lg" className="fixed-bottom">
							<Col md={3} xs={2} className="text-center">
								<Link href="/transactions">
							  		<h3 role="button" className={router.pathname === "/transactions" ? "text-info" : "text-secondary"}><IoIcons.IoMdWallet /></h3>			  		
							  	</Link>
							  	<BrowserView>
							  		<small className="text-center">Transactions</small>
							  	</BrowserView>
						  	</Col>
						  	<Col md={2} xs={2} className="text-center">
								<Link href="/reports">
									<h3 role="button" className={router.pathname === "/reports" ? "text-info" : "text-secondary"}><FaIcons.FaChartPie /></h3>
								</Link>
								<BrowserView>
									<small className="text-center">Reports</small>
								</BrowserView>
							</Col>
							<Col md={2} xs={4} className="text-center">
												
							  	<h1 className="text-dark" role="button" onClick={handleShow}><IoIcons.IoIosAddCircle /></h1>
								<BrowserView>
									<small className="text-center">Add Transaction</small>
								</BrowserView>
							</Col>
							<Col md={2} xs={2} className="text-center">	
							    <Link href="/categories">
								  	<h3 role="button" className={router.pathname === "/categories" ? "text-info" : "text-secondary"}><FaIcons.FaDelicious /></h3>
								</Link>
								<BrowserView>
									<small className="text-center">Categories</small>
								</BrowserView>
							</Col>
							<Col md={3} xs={2} className="text-center">
								<Link href="/profile">
								  	<h3 role="button" className={router.pathname === "/profile" ? "text-info" : "text-secondary"}><FaIcons.FaUser /></h3>
								</Link>
								<BrowserView>
									<small className="text-center">Profile</small>
								</BrowserView>							
							</Col>				    	  
						</Navbar>
					</Row>
				</Col>
			</Row>
		</Container>

      	<Modal show={show} onHide={handleClose} animation={false} onShow={selectCategory}>
	        <Modal.Body>      
	        	<Modal.Title className="text-center py-3">Add Transaction</Modal.Title>
	        	<Form onSubmit={(e) => addTransaction(e)}>
	        		<Form.Row>        			
		        		<Form.Group as={Col} size="lg" className="py-2">	        						      
					        <Form.Control 
						        as="select"
						        size="lg"
						        value={type}
								onChange={(e) => setType(e.target.value)}								
								required
					        >	
					        	<option>Type</option>
					           	<option>Income</option>
						      	<option>Expense</option>				  
						    </Form.Control>					    		      
					    </Form.Group>				    
					</Form.Row>
					<Form.Row>				    
			    	<Form.Group as={Col} size="lg" className="py-1">					   	
				        <Select
	    					options={filteredCategories}	
	    					size="lg"
	    					isDisabled={disabled}
	    					styles={customStyles}
	    					placeholder={filteredCategories.length > 0 && type !== "Type" ? "Category" : msg}        				
	    					onChange={selected => setCategoryId(selected.value)}

	  					/>			        		      	
			   		</Form.Group>
				    </Form.Row>
				    <Form.Row>
				    	<Form.Group as={Col} size="lg" className="py-1">				      
					      <Form.Control 
					      	type="number" 
					      	placeholder="Amount" 
					      	size="lg"
					     	value={amount}
							onChange={(e) => setAmount(e.target.value)}
							required
					      />
					    </Form.Group>
				    </Form.Row>
				    <Form.Row>
				    	<Form.Group as={Col} size="lg" className="py-1">						
							<Form.Control
								type="date"
								placeholder="Date"
								size="lg"
								value={date}
								onChange={(e) => setDate(e.target.value)}
								required
							/>
						</Form.Group>				
				    </Form.Row>
				  	<Form.Row>							 
						    	      	
					    <Form.Group as={Col} controlId="Notes">				      
					      <Form.Control 
					      	type="text" 
					      	placeholder="Notes" 
					      	size="lg"
					     	value={notes}
							onChange={(e) => setNotes(e.target.value)}							
					      />
					    </Form.Group>
					</Form.Row>
					<Form.Row className="mx-2 align-center">					  
						<Button variant="secondary" onClick={handleClose} className="mx-2 my-3 px-5">
		            		Cancel
		          		</Button>
		          		{isActive ? 
		          			<Button variant="info" type="submit" className="mx-2 my-3 px-5">
					    		Add
					  		</Button>
					  		:
					  		<Button variant="info" type="submit" className="mx-2 my-3 px-5" disabled>
					    		Add
					  		</Button>
		          		}	          		
		          	</Form.Row>
				</Form>
	        </Modal.Body>
      	</Modal>
    </React.Fragment>
  )

}

