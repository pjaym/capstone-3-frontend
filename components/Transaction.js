import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import { Card, Button, Row, Col, Modal, Form } from 'react-bootstrap';
import Spinner from 'react-bootstrap/Spinner';
import Select from 'react-select';
import Moment from 'moment';
import path from 'path';
import Image from 'next/image';
import AppHelper from '../helper/app-helper';
import Alert from '../helper/alert';




export default function Transaction({transaction, reload}) {
    
    const { transactionDate, type, name, amount, notes, categoryIcon, _id, categoryId } = transaction; 
    // console.log(transaction)

    const [upType, setUpType] = useState(type.charAt(0).toUpperCase() + type.slice(1));
    const [upPath, setUpPath] = useState('');
    const [upAmount, setUpAmount] = useState(amount);
    const [upDate, setUpDate] = useState(Moment(new Date(transactionDate)).format("yyyy-MM-DD"));
    const [upNotes, setUpNotes] = useState(notes);
    const [upName, setUpName] = useState(name);
    const [upCategoryId, setUpCategoryId] = useState(categoryId);
    const [isActive, setIsActive] = useState(false);
    const [disabled, setDisabled] = useState(true);
    const [userData, setUserData] = useState({});
    const [userCategory, setUserCategory] = useState([]);
    const [filteredCategories, setfilteredCategories] = useState(userCategory);


    const [show, setShow] = useState(false);
    
    const handleShow = () => setShow(true);
    const handleClose = () => setShow(false);


    // Edit specific transaction
    function editTransaction(e) {

        // console.log("trigger edit transaction")

        e.preventDefault();
        
        let tempDate = Moment(new Date()).format()
        let tempTime = tempDate.substr(10, tempDate.length)

        const options = {
            method: 'PUT',
            headers: { 
                'Content-Type': 'application/json'               
            },
            
            body:JSON.stringify({
                amount: upAmount,
                type: upType.toLowerCase(),
                categoryId: upCategoryId,
                transactionDate: upDate + tempTime,
                notes: upNotes

            })
        }

        if (amount !== "" && type !== "" && categoryId !== "") {

            // console.log(options)
            fetch(`${AppHelper.API_URL}/transactions/${_id}`, options)
            .then(AppHelper.toJSON)
            .then(res => {
               
               if(res) {
                    Alert("Transaction saved successfully", "success")
                    
                    reload()

               }else {
                    Alert("Error found! Please try again", "error")
               } 
                              

            })

            handleClose() 

        } else {

            Alert("Category, Type and Amount are required!", "error")
        }

    }


    // Delete specific transaction
    function deleteTransaction() {

        console.log("trigger delete")
        console.log(_id)

        const options = {
            method: 'DELETE'
        }

        fetch(`${AppHelper.API_URL}/transactions/${_id}`, options)
        .then(AppHelper.toJSON)
        .then(res => {
           
           if(res) {
                Alert("Transaction deleted successfully", "success")
                console.log(res)
                reload()                

           }else {
                Alert("Error found! Please try again", "error")
           }  
                         

        })

        handleClose()
    }

    // View specific transaction
    function viewTransaction() {
        
        // console.log("viewTransaction called")
        const options = {
            headers: { Authorization:  `Bearer ${ AppHelper.getAccessToken() }`}
        }

        fetch(`${AppHelper.API_URL}/users/details`, options)
        .then(AppHelper.toJSON)
        .then(userData => {

            // console.log(userData);
            setUserData(userData);

            if (userData.categories.length > 0) {

                let tempUserCategories = [];
                
                userData.categories.map((userCategory, index) => {

                    fetch(`${AppHelper.API_URL}/categories/${userCategory.categoryId}`)
                    .then(AppHelper.toJSON)
                    .then(category => {
                        
                        category.label = category.name;
                        category.value = category._id; 
                        //console.log(category)
                        tempUserCategories.push(category)                   

                    })
                    // console.log(tempUserCategories)
                        setUserCategory(tempUserCategories);
                        // setfilteredCategories(userCategory);
                        
                }) //end map  
                
                 setfilteredCategories(userCategory.filter(category => {
                    return category.type === upType.toLowerCase();
                    console.log(category)
                                    
                }))
            }
        
        })



    }


    // Onchange of category dropdown
    const handleChange = (e) => {
        setUpCategoryId(e.value);
    }

    useEffect(() => {

       if(userCategory.length > 0) {

            setUpCategoryId('');

            // console.log(upType.toLowerCase())

            setfilteredCategories(userCategory.filter(category => {
                return category.type === upType.toLowerCase();
                console.log(category)
                
            }))
            // console.log(filteredCategories)
        }

    }, [upType])

 

    // If there are changes with the values enable the save button
    useEffect(() => {
        
        if((upDate !== Moment(new Date(transactionDate)).format("yyyy-MM-DD") || upNotes !== notes || upAmount !== amount || upType !== (type.charAt(0).toUpperCase() + type.slice(1)) || upCategoryId !== categoryId) && upType !== 'Type') {
            setIsActive(true);
        }else {
            setIsActive(false);
        }
    
    }, [upType, upAmount, upCategoryId, upNotes,  upDate])


     // Back to current value when the modal is close
    useEffect(() => {

        if (!show) {            
            // setUpCategoryId(categoryId)
            setUpNotes(notes)
            setUpName(name)
            setUpType(type.charAt(0).toUpperCase() + type.slice(1))
            setUpAmount(amount)            
            setUpCategoryId(categoryId)
            setUpDate(Moment(new Date(transactionDate)).format("yyyy-MM-DD")) 
            viewTransaction() 

        } 
        setDisabled(true)  
    }, [show])

   
    useEffect(() => {
        if(userCategory.length === 0) {
            viewTransaction()  
        } 
             
    }, [filteredCategories])

    return (
        <React.Fragment>
            {transaction !== undefined 

                ?  
                        <Col xl={4} lg={6}>                
                        <Card className="bg-light my-3">
                            <Button variant="white" onClick={()=>{ viewTransaction(); handleShow() }}>        
                                {<Card.Body>
                                    <Row>
                                    <Col md={2} xs={3}>
                                    {categoryIcon !== undefined 
                                        ? 
                                            <Image
                                                src={path.resolve('./public', categoryIcon)}
                                                alt="Icon"
                                                width={60}
                                                height={60}
                                            />   
                                        :
                                            null                                            
                                    }                       
                                                        
                                    </Col>
                                    <Col md={6} xs={5}>
                                    <Card.Title className="text-dark text-left">{name}</Card.Title>         
                                    <Card.Text className="small text-muted text-left">{new Date(transactionDate).toDateString()}</Card.Text>
                                    </Col>
                                    <Col xs={4}>
                                    <Card.Title className={type === "expense" ? "float-right text-danger" : "float-right text-info"}>₱ {amount.toLocaleString()}</Card.Title>
                                    </Col>
                                    </Row>
                                </Card.Body>}
                            </Button>
                        </Card>                
                    </Col>
                : 
                    null
            }
            
            <Modal show={show} onHide={handleClose} animation={false} onShow={viewTransaction} onEntering={viewTransaction} onEntered={viewTransaction}>
                <Modal.Body>      
                    <Modal.Title className="text-center py-3">Edit Transaction</Modal.Title>
                        <Form onSubmit={(e) => addTransaction(e)}>
                            <Form.Row>                            
                                <Form.Group as={Col} size="lg" className="py-1">                                  
                                    <Form.Control 
                                        as="select"
                                        size="lg"
                                        selected={upType}
                                        value={upType}
                                        onChange={(e) => setUpType(e.target.value)}                               
                                        required
                                    >   
                                        <option>Type</option>
                                        <option>Income</option>
                                        <option>Expense</option>                  
                                    </Form.Control>                                   
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>                                    
                                <Form.Group as={Col}  size="lg" className="py-1">                 
                                    <Select
                                        options={filteredCategories}    
                                        size="lg"
                                        isDisabled={false}
                                        placeholder={filteredCategories.length === 0 ? "Loading..." : "Category"}
                                        value={filteredCategories.length > 0 
                                ? filteredCategories.find(category => category.value === categoryId) : "Loading..."}    
                                        onChange={handleChange}
                                    />                                  
                                </Form.Group>                 
                            </Form.Row>
                            <Form.Row>
                                    <Form.Group as={Col} controlId="Amount">                      
                                      <Form.Control 
                                        type="number" 
                                        placeholder="Amount" 
                                        size="lg"
                                        value={upAmount}
                                        onChange={(e) => setUpAmount(e.target.value)}
                                        required
                                      />
                                    </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} size="lg" className="py-1">                      
                                    <Form.Control
                                        type="date"
                                        placeholder="Date"
                                        size="lg"
                                        value={upDate}
                                        onChange={(e) => setUpDate(e.target.value)}
                                        required
                                    />
                                </Form.Group>               
                            </Form.Row>
                            <Form.Row>                                                   
                                <Form.Group as={Col} controlId="Notes">                     
                                  <Form.Control 
                                    type="text" 
                                    placeholder="Notes" 
                                    size="lg"
                                    value={upNotes}
                                    onChange={(e) => setUpNotes(e.target.value)}                         
                                  />
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} controlId="Description">                    
                                    <Button variant="secondary" onClick={handleClose} className="mx-1 my-3 px-5">
                                        Cancel
                                    </Button>
                                    {isActive ? 
                                        <Button variant="info" onClick={editTransaction} type="submit" className="mx-2 my-3 px-5">
                                            Save
                                        </Button>
                                        :
                                        <Button variant="info" type="submit" className="mx-2 my-3 px-5" disabled>
                                            Save
                                        </Button>
                                    }
                                    <Button variant="danger" onClick={deleteTransaction} className="mx-1 my-3 px-5">
                                        Delete
                                    </Button>
                                </Form.Group>                                
                            </Form.Row>
                    </Form>
                </Modal.Body>
            </Modal>     
        </React.Fragment>
    )
}