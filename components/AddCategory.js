import React, { useState, useEffect } from 'react';
import Router from 'next/router'
import { Row, Col, Button, ButtonGroup, Form, Container, Navbar } from 'react-bootstrap';
import Modal from 'react-bootstrap/Modal';
import Image from 'next/image';
import { useRouter } from 'next/router';
import path from  'path';
import Link from 'next/link';
import { BrowserView } from "react-device-detect";
import * as IoIcons from 'react-icons/io';
import * as FaIcons from 'react-icons/fa';
import AppHelper from '../helper/app-helper';
import Alert from '../helper/alert';
import fetchIcons from '../helper/fetchIcons';
import Icon from  '../components/Icon';
import Categories from '../pages/categories';
import iconData from  '../data/icon';


export default function AddCategory({ reload }) {


	const [name, setName] = useState('');
	const [type, setType] = useState('');
	const [iconPath, setIconPath] = useState('/icons/default.png');
	const [isActive, setIsActive] = useState(false);
	const [router, setRouter] = useState(useRouter());

	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	const [showIco, setShowIco] = useState(false);

	const handleCloseIco = () => setShowIco(false);
	const handleShowIco = () => setShowIco(true);


    function getIcon(path) {

    	// console.log(`nagrun agad yun function ${iconPath}`)
    	setIconPath(path)
    	
    }

    // console.log(iconPath)

    // console.log(iconData)

    const icons = iconData.map((icon, index) => {
		// console.log(icon.path);

		return(
			<Icon key={index} iconPath={icon} get={getIcon} close={handleCloseIco} />
		)
		
	})

    

    // Add a category
     async function addCategory(e) {
    	e.preventDefault();

    	//check if values are successfully binded
		// console.log(name);
		// console.log(type);

		const options = {
            method: 'POST',
            headers: { 
            	'Content-Type': 'application/json',
            	'Authorization': `Bearer ${AppHelper.getAccessToken()}`
            },
            body:JSON.stringify({
                name: name,
                type: type.toLowerCase(),
                iconPath: iconPath
            })
        }

        // console.log(options)

		fetch(`${AppHelper.API_URL}/categories`, options)
        .then(AppHelper.toJSON)
        .then(data => {
            if(data) {
            	console.log("Category added successfully")
            	Alert("Category added successfully", "success")
            	console.log(data)
            	reload()

            }else {
            	console.log("Error found")
            	Alert("Error found! Please try again", "error")
            }
        })
        
        //await data()
       //await location.assign('/categories')
  
       await handleClose()       
        //Router.push('./categories')
        setName('')
        setType('')
        setIconPath('/icons/default.png')
    
    }


	useEffect(() => {
		if(name !== '' && type !== '' && type !== 'Category Type') {
			setIsActive(true);
		}else {
			setIsActive(false);
		}
		
	}, [name, type])

	useEffect(() => {

		if (iconPath !== '/icons/default.png') {
			console.log("icon selected")
			handleCloseIco()

		}else {

			console.log('wala pang naseselect')
			setIconPath('/icons/default.png')
		}

	}, [iconPath])

	useEffect(() => {

		if(!show) {

			setIconPath('/icons/default.png')
			setType('Type')
			setName('')
		}
		
	}, [show])

		

  return (  	
    <React.Fragment>
      	<Container className="w-50">
			<Row>
				<Col md={9} xs={12}>
					<Row>				
						<Navbar bg="light" expand="lg" className="fixed-bottom">
							<Col md={3} xs={2} className="text-center">
								<Link href="/transactions">
							  		<h3 role="button" className={router.pathname === "/transactions" ? "text-info" : "text-secondary"}><IoIcons.IoMdWallet /></h3>			  		
							  	</Link>
							  	<BrowserView>
							  		<small className="text-center">Transactions</small>
							  	</BrowserView>
						  	</Col>
						  	<Col md={2} xs={2} className="text-center">
								<Link href="/reports">
									<h3 role="button" className={router.pathname === "/reports" ? "text-info" : "text-secondary"}><FaIcons.FaChartPie /></h3>
								</Link>
								<BrowserView>
									<small className="text-center">Reports</small>
								</BrowserView>
							</Col>
							<Col md={2} xs={4} className="text-center">
												
							  	<h1 className="text-dark" role="button" onClick={handleShow}><IoIcons.IoIosAddCircle /></h1>
								<BrowserView>
									<small className="text-center">Add Category</small>
								</BrowserView>
							</Col>
							<Col md={2} xs={2} className="text-center">	
							    <Link href="/categories">
								  	<h3 role="button" className={router.pathname === "/categories" ? "text-info" : "text-secondary"}><FaIcons.FaDelicious /></h3>
								</Link>
								<BrowserView>
									<small className="text-center">Categories</small>
								</BrowserView>
							</Col>
							<Col md={3} xs={2} className="text-center">
								<Link href="/profile">
								  	<h3 role="button" className={router.pathname === "/profile" ? "text-info" : "text-secondary"}><FaIcons.FaUser /></h3>
								</Link>
								<BrowserView>
									<small className="text-center">Profile</small>
								</BrowserView>							
							</Col>				    	  
						</Navbar>
					</Row>
				</Col>
			</Row>
		</Container>
      <Modal show={show} onHide={handleClose} animation={false}>
        <Modal.Body>      
        	<Modal.Title className="text-center py-3">Add Category</Modal.Title>
        	<Form onSubmit={(e) => addCategory(e)}>
        		<Row className="mx-1">
        		<Button variant="white" onClick={handleShowIco} className="py-0 px-0 my-2 mx-2">
	            	<Image src={path.resolve('./public', iconPath)} alt="Icon" width={50} height={50} />
	          	</Button> 
	        	      			
		    		<Form.Group as={Col} size="lg" className="py-2">			      
				        <Form.Control 
					        as="select"
					        size="lg"
					        value={type}
							onChange={(e) => setType(e.target.value)}
							required
				        >	
				        	<option>Category Type</option>
				           	<option>Income</option>
					      	<option>Expense</option>				  
					    </Form.Control>		      
				    </Form.Group>
				 </Row>      		     	      	
			    <Form.Group as={Col} size="lg">				      
			      <Form.Control 
			      	type="text" 
			      	placeholder="Category Name" 
			      	size="lg"
			     	value={name}
					onChange={(e) => setName(e.target.value)}
					required
			      />
			    </Form.Group>				
				<Form.Row className="mx-2 align-center">					  
					<Button variant="secondary" onClick={handleClose} className="mx-2 my-3 px-5">
	            		Cancel
	          		</Button>
	          		{isActive ? 
	          			<Button variant="info" type="submit" className="mx-2 my-3 px-5">
				    		Add
				  		</Button>
				  		:
				  		<Button variant="info" type="submit" className="mx-2 my-3 px-5" disabled>
				    		Add
				  		</Button>
	          		}
	          		
	          	</Form.Row>
			</Form>
        </Modal.Body>
      </Modal> 
      <Modal show={showIco} onHide={handleCloseIco} animation={false}>        
        <Modal.Title className="text-center py-3">Select Icon</Modal.Title>       
        <Modal.Body>
          {icons}
        </Modal.Body>       
      </Modal>     
    </React.Fragment>
  );

}

