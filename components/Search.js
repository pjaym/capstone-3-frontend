import React, { useState, useEffect } from 'react';
import { Form, Col } from  'react-bootstrap';
import * as FaIcons from 'react-icons/fa';

export default function Search({data, search, show, reload}) {
	
	// console.log(data);

	const [searchedData, setSearchedData] = useState(data)
	const [searchData,  setSearchData] = useState('')

	/*useEffect(() => {
		setSearchedData(data)
	}, [])
	console.log(searchedData)*/

	useEffect(() => {
		console.log(data)

		if (data !== undefined && data.length > 0 && searchData.length > 2) {			

			//let searchRegex = new RegExp(searchData, "i")

			const searchResult = data.filter(data => {
				return ((data.name.toLowerCase().includes(searchData.toLowerCase()) || data.notes.toLowerCase().includes(searchData.toLowerCase())) && (!data.isDeleted) );
				

			})
			setSearchedData(searchResult)
			search(searchedData)
				
			
		}else if (searchData.length === 0){
			reload()
			search(data)		
		}else if(searchedData.length === 0) {
			search(searchedData)			
		}



	}, [searchData])
	

	useEffect(() => {

		search(searchedData)

	}, [searchedData])

	

	return(
		<Col md={9}>	
			<Form.Group>
			    <Form.Control 
			    	type="text" 
			    	placeholder="Search" 
			    	className=" mr-sm-2"
			    	value={searchData}
			    	onChange={(e) => setSearchData(e.target.value)}
			    />
			</Form.Group>
		</Col>	
	)
}