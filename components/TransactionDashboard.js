import React, { useState, useEffect } from  'react';
import { Jumbotron, Container, Card, Row, Col } from 'react-bootstrap';
import * as BsIcons from 'react-icons/bs';
import * as FaIcons from 'react-icons/fa';
import * as RiIcons from 'react-icons/ri';
import Loader from  '../components/Loader';

export default function TransactionDashboard({userTransaction, startDate, endDate}) {

  const [transactions,setTransactions] = useState(userTransaction)
  const [totalExpense, setTotalExpense] = useState(0)
  const [totalIncome, setTotalIncome] = useState(0)
  const [totalTransactions, setTotalTransactions] = useState(0)
  const [totalBalance, setTotalBalance] = useState(0) 

  useEffect(() => {

    if(userTransaction !== undefined && userTransaction.length > 0 && startDate === undefined && endDate === undefined) {
      startDate = new Date(userTransaction[userTransaction.length - 1].transactionDate).getTime()
      endDate = new Date(userTransaction[0].transactionDate).getTime()

      // console.log(userTransaction[0].transactionDate)
      // console.log(userTransaction[userTransaction.length - 1].transactionDate)

    }

    if(userTransaction !== undefined && userTransaction.length > 0 && startDate !== undefined && endDate !== undefined) {

        let income = 0;
        let expense = 0;
        let count =  0;        

        userTransaction.map(transaction => {          

            if(transaction.type  === 'income' && !transaction.isDeleted && new Date(transaction.transactionDate).getTime() >= startDate && new Date(transaction.transactionDate).getTime() <= endDate) {             

             income += transaction.amount

            }else if (transaction.type  === 'expense' && !transaction.isDeleted && new Date(transaction.transactionDate).getTime() >= startDate && new Date(transaction.transactionDate).getTime() <= endDate) {
              expense += transaction.amount
            }

            if(!transaction.isDeleted && new Date(transaction.transactionDate).getTime() >= startDate && new Date(transaction.transactionDate).getTime() <= endDate) {
              count++
            }
        })

        setTotalIncome(income);
        setTotalExpense(expense);
        setTotalTransactions(count);
        setTotalBalance(income-expense);

    }else {

      setTotalIncome(0);
      setTotalExpense(0);
      setTotalTransactions(0);
      setTotalBalance(0);

    }
    
  }, [userTransaction, startDate, endDate])
  
  
  
  // console.log(userTransaction)
  // console.log(totalExpense)
  // console.log(totalIncome)
  // console.log(totalTransactions)
  // console.log(totalBalance)


  return (
    <React.Fragment>
    	
		  <Container className="py-3 h-60" fluid>
        <Row>
            <Col md={3}>
      		    <Card                
                className="mb-2"
              >   
                <Card.Body className="text-success">
                <Row>
                    <Col md={9}>
                  <Card.Title className="display-4" style={{fontSize: "2.3rem"}}>₱ {totalBalance.toLocaleString()}</Card.Title>
                  </Col>
                    <Col md={3}>
                      <Card.Text className="display-5 text-right text-success" style={{fontSize: "2.3rem"}}> <FaIcons.FaCoins /> </Card.Text>
                    </Col>
                    </Row>
                  <Card.Text>
                    Total Balance
                  </Card.Text>
                </Card.Body>
              </Card>
          </Col> 
          <Col md={3}>
              <Card    
                
                className="mb-2"
              >   
                <Card.Body className="text-info">
                  <Row>
                    <Col md={9}>
                      <Card.Title className="display-4" style={{fontSize: "2.3rem"}}>₱ {totalIncome.toLocaleString()} </Card.Title>
                    </Col>
                    <Col md={3}>
                      <Card.Text className="display-5 text-right" style={{fontSize: "2.3rem"}}> <BsIcons.BsGraphUp /> </Card.Text>
                    </Col>
                    </Row>
                    <Card.Text>
                      Total Income
                    </Card.Text>
                </Card.Body>
              </Card>
          </Col>
          <Col md={3}>
              <Card    
                
                className="mb-2"
              >   
                <Card.Body className="text-danger">
                <Row>
                    <Col md={9}>
                  <Card.Title className="display-4" style={{fontSize: "2.3rem"}}>₱ {totalExpense.toLocaleString()}</Card.Title>
                  </Col>
                    <Col md={3}>
                      <Card.Text className="display-5 text-right" style={{fontSize: "2.3rem"}}> <BsIcons.BsGraphDown /> </Card.Text>
                    </Col>
                    </Row>
                  <Card.Text>
                    Total Expense
                  </Card.Text>
                </Card.Body>
              </Card>
          </Col>
          <Col md={3}>
              <Card className="mb-2">   
                <Card.Body className="text-info">
                <Row>
                    <Col md={9}>
                  <Card.Title className="display-4" style={{fontSize: "2.3rem"}}>{totalTransactions.toLocaleString()}</Card.Title>
                  </Col>
                    <Col md={3}>
                      <Card.Text className="display-5 text-right" style={{fontSize: "2.3rem"}}> <FaIcons.FaLayerGroup /> </Card.Text>
                    </Col>
                    </Row>
                  <Card.Text>
                    Total Transactions
                  </Card.Text>
                </Card.Body>
              </Card>
          </Col>               
      </Row>
		  </Container>
		
    </React.Fragment>

  )
}

