import React, { useState, useEffect } from 'react';
import { Form, Col } from  'react-bootstrap';

export default function Search({data, filter, reload}) {
	
	// console.log(data);

	const [filteredData, setFilteredData] = useState(data)
	const [filterData,  setFilterData] = useState('')

	// useEffect(() => {
	// 	setFilteredData(data)
	// }, [])

	//console.log(filteredData)

	useEffect(() => {
		console.log(data)

		if (data !== undefined && data.length > 0 && filterData !== 'Type') {			

			setFilteredData(data.filter(data => {

				return ((data.type.toLowerCase() == filterData.toLowerCase()) && (!data.isDeleted))		

			}))			
			
			console.log(filterData)
			console.log(filteredData)			
			
		}

		if (filterData === "Type"){
			setFilteredData([])
			reload()					
		}

	}, [filterData])

	useEffect(() => {

		filter(filteredData)
		console.log(filteredData)

	}, [filteredData])

	

	return(
		<Col md={3}>		
			<Form.Group as={Col} controlId="formGridState">
		      	<Form.Control 
		      		as="select"
		      		value={filterData}
		      		onChange={(e) => setFilterData(e.target.value)}
		      	>
			        <option>Type</option>
			        <option>Income</option>
			        <option>Expense</option>
		      	</Form.Control>
		    </Form.Group>
	    </Col>	
	)
}