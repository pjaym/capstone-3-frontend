import React from 'react';

// Creates a context object
const UserContext = React.createContext();

// Provider commponent that allows to provide the context object with states and functions
export const UserProvider = UserContext.Provider;

export default UserContext;


// empty object - React.createContext()
/*UserContext  {
	user: ""
	setUser: () =. {}
	unsetUser: () =. {}
}*/