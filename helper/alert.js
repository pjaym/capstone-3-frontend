import Swal from 'sweetalert2';

export default  function SuccessAlert(msg, ico){  


  const Toast = Swal.mixin({
    toast: true,
    position: 'top',
    width: 1200,
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    customClass: 'swal2-wide',
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  Toast.fire({
    icon: `${ico}`,
    title: `${msg}`

  })

  
  
}