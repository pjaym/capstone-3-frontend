import useSWR from 'swr';

export default  function fetchIcons() {

  // Get Icon Url in the file path 
  const fetcher = (url) => fetch(url).then((res) => res.json());
    const { data } = useSWR('/api/get-icons', fetcher);
    return data ? data : 'No available icons!';
}



